class checkList {
    constructor() {
        // constructor(phoneNum, codeNum) {
        // this.phoneNum = phoneNum;
        // this.codeNum = codeNum;
    }
    checkName(name) {
        var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
        if (name == '') {
            alert('请输入姓名')
            return;
        }
        if (!reg.test(name)) {
            alert('名字格式不正确')
            return false;
        } else {
            return true;
        }
    }
    checkPhoneNum(phoneNum) {
        if (phoneNum == '') {
            alert('请输入手机号');
            return;
        }
        if (!/^1[0-9][0-9]\d{8}$/.test(phoneNum)) {
            alert('请输入正确的手机号码');
            return false;
        } else {
            return true;
        }
    }
    checkNumNotNull(num, alerTxt) {
        if (num == '' || num == 0) {
            alert(alerTxt || '数量不能为空');
            return false;
        } else {
            return true;
        }
    }
    checkCodeNum(codeNum, maxLen = 6, alerTxt) {
        if (codeNum == '' || codeNum.length != maxLen) {
            alert(alerTxt || '请输入验证码');
            return false;
        } else {
            return true;
        }
    }

    //省市联动判断是否取值obj={'选择省份':province}
    checkAppointmentCounter(obj) {
        for (let item in obj) {
            // console.log(item, obj[item])
            if (item == '' || item == obj[item]) {
                alert('请' + obj[item]);
                return false;
            }
        }

        return true;
    }
    checkPrivacy(status) {
        if (!status) {
            alert('请同意隐私条款');
            return false;
        } else {
            return true;
        }
    }
}

var $An = {
    //淡入
    fadeIn(e, {
        frameSpeed = 20,
        callback,
        fadeInOpacityNum
    } = {}) { //ref
        let _this = this;
        let _style = e.currentStyle || getComputedStyle(e)

        // console.log('样式', _style.display)
        //提前return
        if (e.style.fadeOutAn) return;
        if (_style.display == 'block' && _style.opacity == 1) return;

        e.style.fadeInAn = true;
        e.style.opacity ?
            ((e.style.opacity = fadeInOpacityNum.next().value) >= 1 ?

                (e.style.fadeInAn = false, callback && callback.bind(this)()) :
                setTimeout(() => {
                    _this.fadeIn(e, {
                        frameSpeed,
                        callback,
                        fadeInOpacityNum
                    })
                }, frameSpeed)) :
            (fadeInOpacityNum = _this.fadeInOpacity(e.style.opacity, 10),
                e.style.opacity = fadeInOpacityNum.next().value,
                e.style.display = "block",
                _this.fadeIn(e, {
                    frameSpeed,
                    callback,
                    fadeInOpacityNum
                }))
    },
    * fadeInOpacity(val, maxVal) {
        var index = val * 10;
        while (index <= maxVal)
            yield(index++) / 10;
    },
    //淡出,多参数,回调及this绑定
    fadeOut(e, {
        frameSpeed = 20,
        callback
    } = {}) { //ref
        let _this = this;
        let _style = e.currentStyle || getComputedStyle(e)

        if (e.style.fadeInAn) return;
        if (_style.display == 'none') return;

        e.style.fadeOutAn = true;
        e.style.opacity ?
            ((e.style.opacity -= 0.1) <= 0 ?

                (e.style.display = "none", e.style.opacity = "", e.style.fadeOutAn = false, callback && callback
                    .bind(this)()) :
                setTimeout(() => {
                    _this.fadeOut(e, {
                        frameSpeed,
                        callback
                    })
                }, frameSpeed)) :
            (e.style.opacity = 1, _this.fadeOut(e, {
                frameSpeed,
                callback
            }))
    },
}

var $Dom = {
    getStyle(e, style) {
        let _style = e.currentStyle || getComputedStyle(e)
        return _style[style];
    },
}

var $url = {
    getQueryParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
            results = regex.exec(location.search);
        return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    },
    getTime() {
        // Math.round(new Date().getTime().toString() / 1000).toString()
        return Math.floor((new Date()).getTime() / 1000);
    }
}

// class changeRouter {
//     // new Vue.prototype.Utils.changeRouter(router)
//     constructor(router) {
//         this.router = router
//     }
//     push(obj, way = "replace") {
//         // push replace
//         this.router[way](obj)
//     }
// }

var $router = {
    init(router) {
        this.router = router
        return () => {
            console.warn('Utils.$router is already init')
        }
    },
    push(obj, way = "replace") {
        // push replace
        this.router[way](obj)
    }
}

var $MethodFrequencyCheck = {
    /**
     * 如果超过 over_time/1000 直接执行，否则延迟' +delay_time / 1000 +'s执行
     * @param  {} over_time=2000
     * @param  {} delay_time=2000
     */
    init(over_time = 2000, delay_time = 2000) {
        // this.timer = null;
        this.begin = new Date().getTime();
        this.over_time = over_time;
        this.delay_time = delay_time;
        this.methodArr = [];
    },
    hasMethod(method) {
        if (!method) {
            console.warn('$MethodFrequencyCheck method is not found')
            return false
        } else {
            return true
        }
    },
    saveMethodInfo(method) {

        console.log(method.prototype)
            //存入哈希
        if (!this.methodArr[method.name]) {
            this.methodArr[method.name] = {
                'method': method,
                'timer': null,
                'begin': this.begin
            }
        }
    },
    /**
     * 节流
     * @param  {} method
     * @param  {} ...args
     */
    throttle(method, ...args) {
        if (!this.hasMethod(method)) return
        this.saveMethodInfo(method)

        let methodItem = this.methodArr[method.name]
        let current = new Date().getTime();
        if (current - methodItem.begin >= this.over_time) {
            methodItem.method.apply(this, args);
            methodItem.begin = current;
        }
    },
    throttleBoolean(over_time) {
        let current = new Date().getTime();
        let _over_time = over_time || this.over_time
        if (current - this.begin >= _over_time) {
            this.begin = current;
            return true
        } else {
            return false
        }
    },
    /**
     * 防抖 最后调用触发
     * @param  {} method
     * @param  {} ...args
     */
    debounce(method, ...args) {
        if (!this.hasMethod(method)) return
        this.saveMethodInfo(method)

        let methodItem = this.methodArr[method.name]
        let current = new Date().getTime();
        clearTimeout(methodItem.timer);

        methodItem.timer = setTimeout(() => {
            methodItem.method.apply(this, args);
        }, this.delay_time);
    },
}

export default {
    checkList,
    $An,
    $Dom,
    $url,
    $router,
    $MethodFrequencyCheck,
}