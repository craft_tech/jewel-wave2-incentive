import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: { //全局访问,取值
        userInfo: {}, //this.$store.state.userInfo
        changeNum: 0,
        videoLoad: false,
    },
    getters: { //类似computed监听变化
        UserInfo(state) {
            return state.userInfo
        },
        changeNum() {
            return state.changeNum
        },
    },
    mutations: {
        setUserInfo(state, params) { //改变state初始值，赋值
            //this.$store.commit('setUserInfo',{})
            Object.assign(state.userInfo, params)
        },
        setChangeNum(state, sum) {
            state.changeNum += sum;
        },
    },
    actions: { //异步触发mutations里面的方法
        getChangeNum(context, num) { //this.$store.dispatch('getChangeNum',1)
            context.commit('setChangeNum', num)
        }
    }
})

// const store = new Vuex.Store({
//     state,
//     getters,
//     mutations,
//     actions
// });
// export default store;