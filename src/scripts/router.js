import Vue from "vue";
import Router from "vue-router";
import Vue_GLOBAL from '@/scripts/global_variable.js'
import pageConfig from '@/scripts/pageConfig.js'
Vue.use(Router);

// export default new Router({
const router = new Router({
    mode: "hash",
    routes: [{
            path: "/",
            name: "entry",
            component: resolve => {
                // require(["@/pages/pageBox/pageBox.vue"], resolve);
                require(["@/pages/entry.vue"], resolve);
            },
            meta: {
                keepAlive: true,
                from: '',
            },
        },
        {
            path: "/listQiyu",
            name: "listQiyu",
            component: resolve => {
                require(["@/pages/listQiyu/listQiyu.vue"], resolve);
            },
            meta: {
                keepAlive: true,
                from: '',
            },
        },
        {
            path: "/listDiscount",
            name: "listDiscount",
            component: resolve => {
                require(["@/pages/listDiscount/listDiscount.vue"], resolve);
            },
            meta: {
                keepAlive: true,
                from: '',
            },
        },
    ]
});

router.beforeEach((to, from, next) => {
    // if (to.path == '/') {
    //     next({
    //         name: 'home'
    //     })
    //     return
    // }

    // console.log("router>>>>>from", from, " to>>>>", to);
    if (to.matched.length === 0) {
        from.name ? next({
            name: from.name
        }) : next("/"); //先尝试匹配本页的上一级，否则根路由
    } else {
        if (to.matched.some(record => record.meta.requireAuth)) { // 判断该路由是否需要登录权限
            if (localStorage.user_id) { // 判断当前的user_id是否存在 ； 登录存入的user_id
                next();
            } else {
                next({
                    path: '/login',
                    query: {
                        redirect: to.fullPath
                    } // 将要跳转路由的path作为参数，传递到登录页面
                })
            }
        } else {
            to.meta.from = from.name;
            next();
        }
    }

    // if (to.matched.length === 0) {
    //     from.name ? next({
    //         name: from.name
    //     }) : next("/"); //先尝试匹配本页的上一级，否则根路由
    // } else {
    //     next(); //匹配到正确跳转
    // }
});
router.afterEach((to, from) => {
    // console.log("router>>>>>from", from, " to>>>>", to)

    //进入页面 判断是否可滑动
    let nextPageConfig = pageConfig[to['name']]
    if (nextPageConfig) {
        if (nextPageConfig.canScroll) {
            Vue_GLOBAL.data.preventDefault = false
            Vue_GLOBAL.data.passive = false
        } else {
            Vue_GLOBAL.data.preventDefault = true
            Vue_GLOBAL.data.passive = true
        }
    }
})

export default router;